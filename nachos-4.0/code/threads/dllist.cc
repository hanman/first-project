#include "dllist.h"
#include<iostream>
using namespace std;

void DLList::Prepend(void *item) {
	int k;
	if (first == NULL) {
		k = (int)(rand()*1.0 / RAND_MAX) * 100;
		DLLElement *element = new DLLElement(item, k);
		element->item = item;
		first = element;
		last = element;
		first->next = NULL;
		first->prev = NULL;
	}
	else {
		k = first->key - 1;
		DLLElement *element = new DLLElement(item, k);
		element->item = item;
		element->next = first;
		element->prev = NULL;
		first->prev = element;
		first = element;
	}
}

void DLList::Append(void *item) {
	int k;
	if (last == NULL) {
		k = (int)(rand()*1.0 / RAND_MAX) * 100;
		DLLElement *element = new DLLElement(item, k);
		element->item = item;
		first = element;
		last = element;
		first->next = NULL;
		first->prev = NULL;
	}
	else {
		k = last->key - 1;
		DLLElement *element = new DLLElement(item, k);
		element->item = item;
		element->next = NULL;
		element->prev = last;
		last->next = element;
		last = element;
	}
}

void *DLList::Remove(int *keyPtr) {
	if (first == NULL) {
		return NULL;
	}
	else {
		DLLElement *temp;
		temp = first;
		if (first->next != NULL) first = first->next;
		first->prev = NULL;
		temp->next = NULL;
		*keyPtr = temp->key;
		return temp;
	}
}

bool DLList::IsEmpty() {
	if (first == NULL) {
		return false;
	}
	else {
		return true;
	}
}

void DLList::SortedInsert(void *item, int sortKey) {
	DLLElement *element = new DLLElement(item, sortKey);
	DLLElement *temp = first;

	if (first != NULL) {
		while (temp->key <= sortKey) {
			if (temp->next != NULL)
				temp = temp->next;
			else {
				temp->next = element;
				element->next = NULL;
				element->prev = temp;
				for (temp = first; temp != NULL; temp = temp->next) {
					cout << temp->key << " ";
				}
				cout << endl;
				return;
			}
		}


		temp = temp->prev;
		element->next = temp->next;
		element->next->prev = element;
		element->prev = temp;
		temp->next = element;


	}
	else {
		first = element;
		last = element;
		first->next = NULL;
		first->prev = NULL;
	}
	for (temp = first; temp != NULL; temp = temp->next) {
		cout << temp->key << " ";
	}
	cout << endl;
}


void *DLList::SortedRemove(int sortKey) {
	DLLElement *temp = first;
	if (temp->key == sortKey) {
		first = first->next;
		first->prev = NULL;
		temp->next = NULL;
		return temp;
	}
	while (temp->key != sortKey) {
		temp = temp->next;
		if (temp == NULL) {
			return NULL;
		}
	}
	temp->prev->next = temp->next;

	if (temp->next != NULL)
		temp->next->prev = temp->prev;

	temp->next = NULL;
	temp->prev = NULL;

	return temp;
}
